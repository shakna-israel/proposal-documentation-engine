Frontend
========

To overcome the limitations of sphinx's input, it is fairly simple to develop a frontend that allows you to:

#. Input using a WYSIWYG editor.
#. Manage users.
#. Manage document masters.

The proposal for this function is Bottle.
