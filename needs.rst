Needs
=====

#. A central source of documentation
#. Up-To-Date PDF files to allow for printing manuals
#. Easy editing and writing of documentation
#. Cross-platform portability
