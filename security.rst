Security
=========

Security becomes a concern in these areas.

Some concerns are:

#. Not every user should be able to edit every document. There is a need for several levels of security, as well as document masters, individuals who have primary rights over a particular document.
#. Not everyone on the network should be able to access the system. (If using Solution 1).
#. Stability of server.
