.. Proposal - Documentation Engine documentation master file, created by
   sphinx-quickstart on Thu Mar 12 22:53:03 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Proposal: Documentation Engine
==============================

Contents:

.. toctree::
   :maxdepth: 2

   needs
   solution1
   solution2
   sphinx
   frontend
   security
   bottle
