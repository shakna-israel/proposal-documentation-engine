Bottle
======

Bottle.py is a single-file python application that gives you large-scope web technologies.

Bottle is currently used to power websites such as: PyPi, Python.org, GitHub, BitBucket, GitLab, Reddit, and more.

Bottle would allow for:

#. User Management
#. WYSIWYG editing
#. Automating the build process for Sphinx.
