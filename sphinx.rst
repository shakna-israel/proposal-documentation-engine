Sphinx
======

Sphinx is a documentation engine designed for technical documents and individuals.

It supports reStructuredText [RST], a complicated markup language similar in some ways to its younger cousin, Markdown.

However, though writing RST may be complicated, it's output is not:

#. HTML - For natural browser reading.
#. PDF (via LaTeX) - For printing and record keeping.
#. ePub - For eReading devices.
