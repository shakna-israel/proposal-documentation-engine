Solution 1
==========

Python-based sphinx documentation engine with WYSIWYG editor frontend.

Provides:

#. Web-based access to documentation database.
#. Inter-related documents.
#. LaTeX auto-building of PDFs.
#. Easy editing and generating of new documentation.
#. In-built search engine.
