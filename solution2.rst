Solution 2
==========

Python-based sphinx documentation engine with WYSIWYG editor frontend, within a localised browser and context.

Provides:

#. Single-user access to documentation database.
#. Inter-related documents.
#. LaTeX auto-building of PDFs.
#. Easy editing and generating of new documentation.
#. In-built search engine.
